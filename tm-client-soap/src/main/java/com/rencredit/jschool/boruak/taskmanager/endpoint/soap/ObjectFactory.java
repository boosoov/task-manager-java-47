
package com.rencredit.jschool.boruak.taskmanager.endpoint.soap;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.rencredit.jschool.boruak.taskmanager.endpoint.soap package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _AddUserLoginPasswordRole_QNAME = new QName("http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", "addUserLoginPasswordRole");
    private final static QName _AddUserLoginPasswordRoleResponse_QNAME = new QName("http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", "addUserLoginPasswordRoleResponse");
    private final static QName _ClearAllUser_QNAME = new QName("http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", "clearAllUser");
    private final static QName _ClearAllUserResponse_QNAME = new QName("http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", "clearAllUserResponse");
    private final static QName _GetUserById_QNAME = new QName("http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", "getUserById");
    private final static QName _GetUserByIdResponse_QNAME = new QName("http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", "getUserByIdResponse");
    private final static QName _GetUserByLogin_QNAME = new QName("http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", "getUserByLogin");
    private final static QName _GetUserByLoginResponse_QNAME = new QName("http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", "getUserByLoginResponse");
    private final static QName _GetUserList_QNAME = new QName("http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", "getUserList");
    private final static QName _GetUserListResponse_QNAME = new QName("http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", "getUserListResponse");
    private final static QName _LockUserByLogin_QNAME = new QName("http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", "lockUserByLogin");
    private final static QName _LockUserByLoginResponse_QNAME = new QName("http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", "lockUserByLoginResponse");
    private final static QName _RemoveUserById_QNAME = new QName("http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", "removeUserById");
    private final static QName _RemoveUserByIdResponse_QNAME = new QName("http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", "removeUserByIdResponse");
    private final static QName _RemoveUserByLogin_QNAME = new QName("http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", "removeUserByLogin");
    private final static QName _RemoveUserByLoginResponse_QNAME = new QName("http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", "removeUserByLoginResponse");
    private final static QName _Roles_QNAME = new QName("http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", "roles");
    private final static QName _RolesResponse_QNAME = new QName("http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", "rolesResponse");
    private final static QName _UnlockUserByLogin_QNAME = new QName("http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", "unlockUserByLogin");
    private final static QName _UnlockUserByLoginResponse_QNAME = new QName("http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", "unlockUserByLoginResponse");
    private final static QName _EmptyIdException_QNAME = new QName("http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", "EmptyIdException");
    private final static QName _EmptyUserException_QNAME = new QName("http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", "EmptyUserException");
    private final static QName _DeniedAccessException_QNAME = new QName("http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", "DeniedAccessException");
    private final static QName _EmptyLoginException_QNAME = new QName("http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", "EmptyLoginException");
    private final static QName _EmptyPasswordException_QNAME = new QName("http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", "EmptyPasswordException");
    private final static QName _EmptyRoleException_QNAME = new QName("http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", "EmptyRoleException");
    private final static QName _BusyLoginException_QNAME = new QName("http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", "BusyLoginException");
    private final static QName _EmptyHashLineException_QNAME = new QName("http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", "EmptyHashLineException");
    private final static QName _UnknownUserException_QNAME = new QName("http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", "UnknownUserException");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.rencredit.jschool.boruak.taskmanager.endpoint.soap
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link AddUserLoginPasswordRole }
     * 
     */
    public AddUserLoginPasswordRole createAddUserLoginPasswordRole() {
        return new AddUserLoginPasswordRole();
    }

    /**
     * Create an instance of {@link AddUserLoginPasswordRoleResponse }
     * 
     */
    public AddUserLoginPasswordRoleResponse createAddUserLoginPasswordRoleResponse() {
        return new AddUserLoginPasswordRoleResponse();
    }

    /**
     * Create an instance of {@link ClearAllUser }
     * 
     */
    public ClearAllUser createClearAllUser() {
        return new ClearAllUser();
    }

    /**
     * Create an instance of {@link ClearAllUserResponse }
     * 
     */
    public ClearAllUserResponse createClearAllUserResponse() {
        return new ClearAllUserResponse();
    }

    /**
     * Create an instance of {@link GetUserById }
     * 
     */
    public GetUserById createGetUserById() {
        return new GetUserById();
    }

    /**
     * Create an instance of {@link GetUserByIdResponse }
     * 
     */
    public GetUserByIdResponse createGetUserByIdResponse() {
        return new GetUserByIdResponse();
    }

    /**
     * Create an instance of {@link GetUserByLogin }
     * 
     */
    public GetUserByLogin createGetUserByLogin() {
        return new GetUserByLogin();
    }

    /**
     * Create an instance of {@link GetUserByLoginResponse }
     * 
     */
    public GetUserByLoginResponse createGetUserByLoginResponse() {
        return new GetUserByLoginResponse();
    }

    /**
     * Create an instance of {@link GetUserList }
     * 
     */
    public GetUserList createGetUserList() {
        return new GetUserList();
    }

    /**
     * Create an instance of {@link GetUserListResponse }
     * 
     */
    public GetUserListResponse createGetUserListResponse() {
        return new GetUserListResponse();
    }

    /**
     * Create an instance of {@link LockUserByLogin }
     * 
     */
    public LockUserByLogin createLockUserByLogin() {
        return new LockUserByLogin();
    }

    /**
     * Create an instance of {@link LockUserByLoginResponse }
     * 
     */
    public LockUserByLoginResponse createLockUserByLoginResponse() {
        return new LockUserByLoginResponse();
    }

    /**
     * Create an instance of {@link RemoveUserById }
     * 
     */
    public RemoveUserById createRemoveUserById() {
        return new RemoveUserById();
    }

    /**
     * Create an instance of {@link RemoveUserByIdResponse }
     * 
     */
    public RemoveUserByIdResponse createRemoveUserByIdResponse() {
        return new RemoveUserByIdResponse();
    }

    /**
     * Create an instance of {@link RemoveUserByLogin }
     * 
     */
    public RemoveUserByLogin createRemoveUserByLogin() {
        return new RemoveUserByLogin();
    }

    /**
     * Create an instance of {@link RemoveUserByLoginResponse }
     * 
     */
    public RemoveUserByLoginResponse createRemoveUserByLoginResponse() {
        return new RemoveUserByLoginResponse();
    }

    /**
     * Create an instance of {@link Roles }
     * 
     */
    public Roles createRoles() {
        return new Roles();
    }

    /**
     * Create an instance of {@link RolesResponse }
     * 
     */
    public RolesResponse createRolesResponse() {
        return new RolesResponse();
    }

    /**
     * Create an instance of {@link UnlockUserByLogin }
     * 
     */
    public UnlockUserByLogin createUnlockUserByLogin() {
        return new UnlockUserByLogin();
    }

    /**
     * Create an instance of {@link UnlockUserByLoginResponse }
     * 
     */
    public UnlockUserByLoginResponse createUnlockUserByLoginResponse() {
        return new UnlockUserByLoginResponse();
    }

    /**
     * Create an instance of {@link EmptyIdException }
     * 
     */
    public EmptyIdException createEmptyIdException() {
        return new EmptyIdException();
    }

    /**
     * Create an instance of {@link EmptyUserException }
     * 
     */
    public EmptyUserException createEmptyUserException() {
        return new EmptyUserException();
    }

    /**
     * Create an instance of {@link DeniedAccessException }
     * 
     */
    public DeniedAccessException createDeniedAccessException() {
        return new DeniedAccessException();
    }

    /**
     * Create an instance of {@link EmptyLoginException }
     * 
     */
    public EmptyLoginException createEmptyLoginException() {
        return new EmptyLoginException();
    }

    /**
     * Create an instance of {@link EmptyPasswordException }
     * 
     */
    public EmptyPasswordException createEmptyPasswordException() {
        return new EmptyPasswordException();
    }

    /**
     * Create an instance of {@link EmptyRoleException }
     * 
     */
    public EmptyRoleException createEmptyRoleException() {
        return new EmptyRoleException();
    }

    /**
     * Create an instance of {@link BusyLoginException }
     * 
     */
    public BusyLoginException createBusyLoginException() {
        return new BusyLoginException();
    }

    /**
     * Create an instance of {@link EmptyHashLineException }
     * 
     */
    public EmptyHashLineException createEmptyHashLineException() {
        return new EmptyHashLineException();
    }

    /**
     * Create an instance of {@link UnknownUserException }
     * 
     */
    public UnknownUserException createUnknownUserException() {
        return new UnknownUserException();
    }

    /**
     * Create an instance of {@link UserDTO }
     * 
     */
    public UserDTO createUserDTO() {
        return new UserDTO();
    }

    /**
     * Create an instance of {@link AbstractDTO }
     * 
     */
    public AbstractDTO createAbstractDTO() {
        return new AbstractDTO();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddUserLoginPasswordRole }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "addUserLoginPasswordRole")
    public JAXBElement<AddUserLoginPasswordRole> createAddUserLoginPasswordRole(AddUserLoginPasswordRole value) {
        return new JAXBElement<AddUserLoginPasswordRole>(_AddUserLoginPasswordRole_QNAME, AddUserLoginPasswordRole.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddUserLoginPasswordRoleResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "addUserLoginPasswordRoleResponse")
    public JAXBElement<AddUserLoginPasswordRoleResponse> createAddUserLoginPasswordRoleResponse(AddUserLoginPasswordRoleResponse value) {
        return new JAXBElement<AddUserLoginPasswordRoleResponse>(_AddUserLoginPasswordRoleResponse_QNAME, AddUserLoginPasswordRoleResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClearAllUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "clearAllUser")
    public JAXBElement<ClearAllUser> createClearAllUser(ClearAllUser value) {
        return new JAXBElement<ClearAllUser>(_ClearAllUser_QNAME, ClearAllUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClearAllUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "clearAllUserResponse")
    public JAXBElement<ClearAllUserResponse> createClearAllUserResponse(ClearAllUserResponse value) {
        return new JAXBElement<ClearAllUserResponse>(_ClearAllUserResponse_QNAME, ClearAllUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUserById }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "getUserById")
    public JAXBElement<GetUserById> createGetUserById(GetUserById value) {
        return new JAXBElement<GetUserById>(_GetUserById_QNAME, GetUserById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUserByIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "getUserByIdResponse")
    public JAXBElement<GetUserByIdResponse> createGetUserByIdResponse(GetUserByIdResponse value) {
        return new JAXBElement<GetUserByIdResponse>(_GetUserByIdResponse_QNAME, GetUserByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUserByLogin }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "getUserByLogin")
    public JAXBElement<GetUserByLogin> createGetUserByLogin(GetUserByLogin value) {
        return new JAXBElement<GetUserByLogin>(_GetUserByLogin_QNAME, GetUserByLogin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUserByLoginResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "getUserByLoginResponse")
    public JAXBElement<GetUserByLoginResponse> createGetUserByLoginResponse(GetUserByLoginResponse value) {
        return new JAXBElement<GetUserByLoginResponse>(_GetUserByLoginResponse_QNAME, GetUserByLoginResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUserList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "getUserList")
    public JAXBElement<GetUserList> createGetUserList(GetUserList value) {
        return new JAXBElement<GetUserList>(_GetUserList_QNAME, GetUserList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUserListResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "getUserListResponse")
    public JAXBElement<GetUserListResponse> createGetUserListResponse(GetUserListResponse value) {
        return new JAXBElement<GetUserListResponse>(_GetUserListResponse_QNAME, GetUserListResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LockUserByLogin }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "lockUserByLogin")
    public JAXBElement<LockUserByLogin> createLockUserByLogin(LockUserByLogin value) {
        return new JAXBElement<LockUserByLogin>(_LockUserByLogin_QNAME, LockUserByLogin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LockUserByLoginResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "lockUserByLoginResponse")
    public JAXBElement<LockUserByLoginResponse> createLockUserByLoginResponse(LockUserByLoginResponse value) {
        return new JAXBElement<LockUserByLoginResponse>(_LockUserByLoginResponse_QNAME, LockUserByLoginResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveUserById }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "removeUserById")
    public JAXBElement<RemoveUserById> createRemoveUserById(RemoveUserById value) {
        return new JAXBElement<RemoveUserById>(_RemoveUserById_QNAME, RemoveUserById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveUserByIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "removeUserByIdResponse")
    public JAXBElement<RemoveUserByIdResponse> createRemoveUserByIdResponse(RemoveUserByIdResponse value) {
        return new JAXBElement<RemoveUserByIdResponse>(_RemoveUserByIdResponse_QNAME, RemoveUserByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveUserByLogin }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "removeUserByLogin")
    public JAXBElement<RemoveUserByLogin> createRemoveUserByLogin(RemoveUserByLogin value) {
        return new JAXBElement<RemoveUserByLogin>(_RemoveUserByLogin_QNAME, RemoveUserByLogin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveUserByLoginResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "removeUserByLoginResponse")
    public JAXBElement<RemoveUserByLoginResponse> createRemoveUserByLoginResponse(RemoveUserByLoginResponse value) {
        return new JAXBElement<RemoveUserByLoginResponse>(_RemoveUserByLoginResponse_QNAME, RemoveUserByLoginResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Roles }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "roles")
    public JAXBElement<Roles> createRoles(Roles value) {
        return new JAXBElement<Roles>(_Roles_QNAME, Roles.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RolesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "rolesResponse")
    public JAXBElement<RolesResponse> createRolesResponse(RolesResponse value) {
        return new JAXBElement<RolesResponse>(_RolesResponse_QNAME, RolesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UnlockUserByLogin }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "unlockUserByLogin")
    public JAXBElement<UnlockUserByLogin> createUnlockUserByLogin(UnlockUserByLogin value) {
        return new JAXBElement<UnlockUserByLogin>(_UnlockUserByLogin_QNAME, UnlockUserByLogin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UnlockUserByLoginResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "unlockUserByLoginResponse")
    public JAXBElement<UnlockUserByLoginResponse> createUnlockUserByLoginResponse(UnlockUserByLoginResponse value) {
        return new JAXBElement<UnlockUserByLoginResponse>(_UnlockUserByLoginResponse_QNAME, UnlockUserByLoginResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyIdException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "EmptyIdException")
    public JAXBElement<EmptyIdException> createEmptyIdException(EmptyIdException value) {
        return new JAXBElement<EmptyIdException>(_EmptyIdException_QNAME, EmptyIdException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyUserException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "EmptyUserException")
    public JAXBElement<EmptyUserException> createEmptyUserException(EmptyUserException value) {
        return new JAXBElement<EmptyUserException>(_EmptyUserException_QNAME, EmptyUserException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeniedAccessException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "DeniedAccessException")
    public JAXBElement<DeniedAccessException> createDeniedAccessException(DeniedAccessException value) {
        return new JAXBElement<DeniedAccessException>(_DeniedAccessException_QNAME, DeniedAccessException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyLoginException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "EmptyLoginException")
    public JAXBElement<EmptyLoginException> createEmptyLoginException(EmptyLoginException value) {
        return new JAXBElement<EmptyLoginException>(_EmptyLoginException_QNAME, EmptyLoginException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyPasswordException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "EmptyPasswordException")
    public JAXBElement<EmptyPasswordException> createEmptyPasswordException(EmptyPasswordException value) {
        return new JAXBElement<EmptyPasswordException>(_EmptyPasswordException_QNAME, EmptyPasswordException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyRoleException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "EmptyRoleException")
    public JAXBElement<EmptyRoleException> createEmptyRoleException(EmptyRoleException value) {
        return new JAXBElement<EmptyRoleException>(_EmptyRoleException_QNAME, EmptyRoleException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BusyLoginException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "BusyLoginException")
    public JAXBElement<BusyLoginException> createBusyLoginException(BusyLoginException value) {
        return new JAXBElement<BusyLoginException>(_BusyLoginException_QNAME, BusyLoginException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyHashLineException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "EmptyHashLineException")
    public JAXBElement<EmptyHashLineException> createEmptyHashLineException(EmptyHashLineException value) {
        return new JAXBElement<EmptyHashLineException>(_EmptyHashLineException_QNAME, EmptyHashLineException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UnknownUserException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "UnknownUserException")
    public JAXBElement<UnknownUserException> createUnknownUserException(UnknownUserException value) {
        return new JAXBElement<UnknownUserException>(_UnknownUserException_QNAME, UnknownUserException.class, null, value);
    }

}
