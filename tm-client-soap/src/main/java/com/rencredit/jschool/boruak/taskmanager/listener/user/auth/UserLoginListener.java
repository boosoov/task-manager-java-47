package com.rencredit.jschool.boruak.taskmanager.listener.user.auth;

import com.rencredit.jschool.boruak.taskmanager.endpoint.soap.*;
import com.rencredit.jschool.boruak.taskmanager.event.ConsoleEvent;
import com.rencredit.jschool.boruak.taskmanager.listener.AbstractListener;
import com.rencredit.jschool.boruak.taskmanager.service.AuthService;
import com.rencredit.jschool.boruak.taskmanager.service.SessionService;
import com.rencredit.jschool.boruak.taskmanager.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class UserLoginListener extends AbstractListener {

//    @Autowired
//    private AuthEndpoint authEndpoint;
//
//    @Autowired
//    private ProjectEndpoint projectEndpoint;
//
//    @Autowired
//    private TaskEndpoint taskEndpoint;
//
//    @Autowired
//    private UserEndpoint userEndpoint;
//
//    @Autowired
//    private AdminEndpoint adminEndpoint;
//
//    @Autowired
//    private AdminUserEndpoint adminUserEndpoint;
//
//    @Autowired
//    private SessionService sessionService;

    @Autowired
    private AuthService authService;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "login";
    }

    @NotNull
    @Override
    public String description() {
        return "Login user in program";
    }

    @Override
    @EventListener(condition = "@userLoginListener.name() == #event.command")
    public void handle(final ConsoleEvent event) throws EmptyLoginException_Exception, UnknownUserException_Exception, DeniedAccessException_Exception {
        System.out.println("Enter login");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("Enter password");
        @NotNull final String password = TerminalUtil.nextLine();

//        SessionService.setMaintain(authEndpoint);
//        SessionService.setMaintain(projectEndpoint);
//        SessionService.setMaintain(taskEndpoint);
//        SessionService.setMaintain(userEndpoint);
//        SessionService.setMaintain(adminEndpoint);
//        SessionService.setMaintain(adminUserEndpoint);
//
//        if(authEndpoint.login(login, password).isSuccess()){
//            List<String> session = sessionService.getSession();
//            if(session == null) {
//                session = SessionService.getListCookieRow(authEndpoint);
//                sessionService.setSession(session);
//            }
//            SessionService.setListCookieRowRequest(authEndpoint, session);
//            SessionService.setListCookieRowRequest(projectEndpoint, session);
//            SessionService.setListCookieRowRequest(taskEndpoint, session);
//            SessionService.setListCookieRowRequest(userEndpoint, session);
//            SessionService.setListCookieRowRequest(adminEndpoint, session);
//            SessionService.setListCookieRowRequest(adminUserEndpoint, session);
//            System.out.println("Login success");
//        } else {
//            System.out.println("Login fail");
//        }

        if(authService.login(login, password)){
            System.out.println("Login success");
        } else {
            System.out.println("Login fail");
        }
    }

}
