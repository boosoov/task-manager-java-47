package com.rencredit.jschool.boruak.taskmanager.restendpoint;

import com.rencredit.jschool.boruak.taskmanager.config.ClientConfiguration;
import com.rencredit.jschool.boruak.taskmanager.dto.TaskDTO;
import com.rencredit.jschool.boruak.taskmanager.endpoint.server.IAuthRestEndpoint;
import com.rencredit.jschool.boruak.taskmanager.endpoint.server.ITaskRestEndpoint;
import com.rencredit.jschool.boruak.taskmanager.marker.IntegrationWithServerTestCategory;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ClientConfiguration.class)
@Category(IntegrationWithServerTestCategory.class)
public class ITaskRestEndpointTest {

    @Autowired
    ITaskRestEndpoint taskRestEndpoint;

    @Autowired
    IAuthRestEndpoint authRestEndpoint;

    @Before
    public void init()  {
        authRestEndpoint.login("test", "test");
    }

    @After
    public void clearAll() {
        authRestEndpoint.logout();
    }

    @Test
    public void test(){
        final TaskDTO task = new TaskDTO();
        task.setDescription("111111");
        taskRestEndpoint.create(task);

        final TaskDTO taskFromWeb = taskRestEndpoint.findOneByIdDTO(task.getId());
        Assert.assertEquals(task.getId(), taskFromWeb.getId());

        Assert.assertTrue(taskRestEndpoint.existsById(task.getId()));

        taskRestEndpoint.deleteOneById(task.getId());
        Assert.assertFalse(taskRestEndpoint.existsById(task.getId()));
    }

}
