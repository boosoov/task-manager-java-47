package com.rencredit.jschool.boruak.taskmanager.endpoint.server;

import com.rencredit.jschool.boruak.taskmanager.dto.TaskDTO;
import com.rencredit.jschool.boruak.taskmanager.service.SessionService;
import feign.Feign;
import feign.okhttp.OkHttpClient;
import okhttp3.JavaNetCookieJar;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.cloud.openfeign.support.SpringDecoder;
import org.springframework.cloud.openfeign.support.SpringEncoder;
import org.springframework.cloud.openfeign.support.SpringMvcContract;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.*;

import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.List;

@RequestMapping("/api/tasks")
public interface ITasksRestEndpoint {

    static ITasksRestEndpoint client(final String baseUrl) {
        final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        final HttpMessageConverters converters = new HttpMessageConverters(converter);
        final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;

        final CookieManager cookieManager = SessionService.getSession();
        cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);

        final okhttp3.OkHttpClient.Builder builder =
                new okhttp3.OkHttpClient().newBuilder();
        builder.cookieJar(new JavaNetCookieJar(cookieManager));

        return Feign.builder()
                .client(new OkHttpClient(builder.build()))
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(ITasksRestEndpoint.class, baseUrl);
    }

    @GetMapping
    List<TaskDTO> getListDTO();

    @GetMapping("/all")
    List<TaskDTO> getListDTOAll();

    @PostMapping
    List<TaskDTO> saveAll(@RequestBody List<TaskDTO> list);

    @GetMapping("/count")
    long count();

    @DeleteMapping("/all")
    void deleteAll();
}
