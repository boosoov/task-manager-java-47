package com.rencredit.jschool.boruak.taskmanager.repository.dto;

import com.rencredit.jschool.boruak.taskmanager.config.WebApplicationConfiguration;
import com.rencredit.jschool.boruak.taskmanager.dto.TaskDTO;
import com.rencredit.jschool.boruak.taskmanager.entity.Project;
import com.rencredit.jschool.boruak.taskmanager.entity.User;
import com.rencredit.jschool.boruak.taskmanager.repository.dto.ITaskRepositoryDTO;
import com.rencredit.jschool.boruak.taskmanager.repository.entity.IProjectRepositoryEntity;
import com.rencredit.jschool.boruak.taskmanager.repository.entity.IUserRepositoryEntity;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.junit.Assert.*;

@WebAppConfiguration
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = WebApplicationConfiguration.class)
public class TaskRepositoryDtoTest {

    @Autowired
    private ITaskRepositoryDTO taskRepository;

    @Autowired
    private IUserRepositoryEntity userRepository;

    @Autowired
    private IProjectRepositoryEntity projectRepository;

    private User user;
    
    @Before
    public void setup() {
        taskRepository.deleteAll();
        userRepository.deleteAll();
        projectRepository.deleteAll();
        user = new User("login", "password");
        userRepository.save(user);
    }

    @Test
    public void testSave() {
        final TaskDTO task = new TaskDTO(user.getId(), "name");
        taskRepository.save(task);
        assertNotNull(taskRepository.findById(task.getId()));
    }

    @Test
    public void testFindById() {
        final TaskDTO task = new TaskDTO(user.getId(), "name");
        taskRepository.save(task);
        final TaskDTO taskFromBase = taskRepository.findById(task.getId()).orElse(null);
        assertNotNull(taskFromBase);
        assertEquals(task.getId(), taskFromBase.getId());
    }

    @Test
    public void testFindByUserIdAndName() {
        final TaskDTO task = new TaskDTO(user.getId(), "name");
        taskRepository.save(task);
        final TaskDTO taskFromBase = taskRepository.findByUserIdAndName(user.getId(), task.getName());
        assertNotNull(taskFromBase);
        assertEquals(task.getId(), taskFromBase.getId());
    }

    @Test
    public void testFindByUserIdAndId() {
        final TaskDTO task = new TaskDTO(user.getId(), "name");
        taskRepository.save(task);
        final TaskDTO taskFromBase = taskRepository.findByUserIdAndId(user.getId(), task.getId());
        assertNotNull(taskFromBase);
        assertEquals(task.getId(), taskFromBase.getId());
    }

    @Test
    public void testExistById() {
        final TaskDTO task = new TaskDTO(user.getId(), "name");
        taskRepository.save(task);
        assertTrue(taskRepository.existsById(task.getId()));
        assertFalse(taskRepository.existsById("34234"));
    }

    @Test
    public void testUpdate() {
        TaskDTO task = new TaskDTO(user.getId(), "name");
        taskRepository.save(task);
        final TaskDTO taskFromBase = taskRepository.findById(task.getId()).orElse(null);
        assertNotNull(taskFromBase);
        assertEquals(task.getName(), taskFromBase.getName());

        task.setName("name2");
        taskRepository.save(task);
        final TaskDTO taskFromBaseWithAnotherLogin = taskRepository.findById(task.getId()).orElse(null);
        assertNotNull(taskFromBaseWithAnotherLogin);
        assertEquals(task.getId(), taskFromBaseWithAnotherLogin.getId());
        assertEquals(task.getName(), taskFromBaseWithAnotherLogin.getName());
    }

    @Test
    public void testFindAll() {
        taskRepository.save(new TaskDTO());
        taskRepository.save(new TaskDTO());
        taskRepository.save(new TaskDTO());
        assertEquals(3, taskRepository.findAll().size());
    }

    @Test
    public void testFindAllByUserId() {
        taskRepository.save(new TaskDTO(user.getId(), "name"));
        taskRepository.save(new TaskDTO(user.getId(), "name"));
        taskRepository.save(new TaskDTO());
        assertEquals(2, taskRepository.findAllByUserId(user.getId()).size());
    }

    @Test
    public void testFindAllByProjectId() {
        taskRepository.save(new TaskDTO(user.getId(), "name"));
        taskRepository.save(new TaskDTO(user.getId(), "name"));
        taskRepository.save(new TaskDTO());

        Project project = new Project();
        projectRepository.save(project);

        TaskDTO task1 = new TaskDTO(user.getId(), "name");
        task1.setProject(project.getId());
        taskRepository.save(task1);
        TaskDTO task2 = new TaskDTO(user.getId(), "name");
        task2.setProject(project.getId());
        taskRepository.save(task2);
        TaskDTO task3 = new TaskDTO(user.getId(), "name");
        task3.setProject(project.getId());
        taskRepository.save(task3);
        TaskDTO task4 = new TaskDTO(user.getId(), "name");
        task4.setProject(project.getId());
        taskRepository.save(task4);

        assertEquals(4, taskRepository.findAllByProject(project.getId()).size());
    }

    @Test
    public void testCount() {
        taskRepository.save(new TaskDTO());
        taskRepository.save(new TaskDTO());
        taskRepository.save(new TaskDTO());
        assertEquals(3, taskRepository.count());
    }

    @Test
    public void testCountAllByUserId() {
        taskRepository.save(new TaskDTO(user.getId(), "name"));
        taskRepository.save(new TaskDTO(user.getId(), "name"));
        taskRepository.save(new TaskDTO());
        assertEquals(2, taskRepository.countAllByUserId(user.getId()));
    }

}
