package com.rencredit.jschool.boruak.taskmanager.api.endpoint.rest;

import com.rencredit.jschool.boruak.taskmanager.dto.ProjectDTO;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.exception.unknown.UnknownUserException;
import org.jetbrains.annotations.Nullable;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

public interface IProjectRestEndpoint {
    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    void create(@RequestBody ProjectDTO project) throws EmptyProjectException, EmptyUserIdException, EmptyLoginException, DeniedAccessException, UnknownUserException;

    @Nullable
    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    ProjectDTO findOneByIdDTO(@PathVariable("id") String id) throws EmptyIdException, EmptyUserIdException, EmptyLoginException, DeniedAccessException, UnknownUserException;

    @GetMapping(value = "/exist/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    boolean existsById(@PathVariable("id") String id) throws DeniedAccessException, UnknownUserException, EmptyUserIdException, EmptyProjectIdException;

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    void deleteOneById(@PathVariable("id") String id) throws DeniedAccessException, UnknownUserException, EmptyUserIdException, EmptyProjectIdException;

}
