package com.rencredit.jschool.boruak.taskmanager.api.endpoint.rest;

import com.rencredit.jschool.boruak.taskmanager.dto.UserDTO;
import com.rencredit.jschool.boruak.taskmanager.dto.status.Result;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyIdException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyLoginException;
import com.rencredit.jschool.boruak.taskmanager.exception.unknown.UnknownUserException;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

public interface IAuthRestEndpoint {
    @GetMapping(value = "/login", produces = MediaType.APPLICATION_JSON_VALUE)
    Result login(
            @RequestParam("username") String username,
            @RequestParam("password") String password
    ) throws EmptyLoginException, UnknownUserException, DeniedAccessException;

    @GetMapping(value = "/session", produces = MediaType.APPLICATION_JSON_VALUE)
    Authentication session();

    @GetMapping(value = "/profile", produces = MediaType.APPLICATION_JSON_VALUE)
    UserDTO profile() throws DeniedAccessException, UnknownUserException, EmptyIdException;

    @GetMapping(value = "/logout", produces = MediaType.APPLICATION_JSON_VALUE)
    Result logout();
}
