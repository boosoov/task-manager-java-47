package com.rencredit.jschool.boruak.taskmanager.exception.empty;

import com.rencredit.jschool.boruak.taskmanager.exception.AbstractException;

public class EmptyProjectIdException extends AbstractException {

    public EmptyProjectIdException() {
        super("Error! Project Id is empty...");
    }

}
