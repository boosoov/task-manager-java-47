package com.rencredit.jschool.boruak.taskmanager.api.endpoint.rest;

import com.rencredit.jschool.boruak.taskmanager.dto.TaskDTO;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyTaskListException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyUserIdException;
import com.rencredit.jschool.boruak.taskmanager.exception.unknown.UnknownUserException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

public interface ITasksRestEndpoint {
    @GetMapping
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    List<TaskDTO> getListDTO() throws EmptyUserIdException, DeniedAccessException, UnknownUserException;

    @GetMapping("/all")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    List<TaskDTO> getListDTOAll();

    @PostMapping
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    List<TaskDTO> saveAll(@RequestBody List<TaskDTO> list) throws DeniedAccessException, UnknownUserException, EmptyTaskListException;

    @GetMapping("/count")
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    long count() throws DeniedAccessException, UnknownUserException;

    @DeleteMapping("/all")
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    void deleteAll() throws EmptyUserIdException, DeniedAccessException, UnknownUserException;
}
