package com.rencredit.jschool.boruak.taskmanager.exception.empty;

import com.rencredit.jschool.boruak.taskmanager.exception.AbstractException;

public class EmptyUserException extends AbstractException {

    public EmptyUserException() {
        super("Error! User is empty...");
    }

}
