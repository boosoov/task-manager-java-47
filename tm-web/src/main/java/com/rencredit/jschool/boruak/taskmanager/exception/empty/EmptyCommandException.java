package com.rencredit.jschool.boruak.taskmanager.exception.empty;

import com.rencredit.jschool.boruak.taskmanager.exception.AbstractException;

public class EmptyCommandException extends AbstractException {

    public EmptyCommandException() {
        super("Error! Command is empty...");
    }

}
